package server;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by alchimik on 02.06.16.
 */
public class JettyServer extends AbstractHandler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, ServletException {
        httpServletResponse.setContentType("text/html; charset=utf-8");
        httpServletResponse.getWriter().println( "<script type=\"text/javascript\">
                function reloadPage()
        {
            window.location.reload()
        }
        </script>");
        httpServletResponse.getWriter().println("<h1>Hello World</h1>");
        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        request.setHandled(true);

    }

    public static void main(String[] args) throws Exception{
        Server server = new Server(8080);
        server.setHandler(new JettyServer());
        server.start();
        server.join();
    }
}
